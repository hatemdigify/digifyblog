@extends('admin/layouts/main')

@section('content')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
		
          <h1>
            {{\Auth::user()->blogname}}
            <small> Home</small>
          
          </h1>
          
		  
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Your Page Content Here -->
		   <div class="row">
			
            
		   <div class="col-lg-12 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{$noofarticles}}</h3>
                  <p>No of  Article</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            
            </div><!-- ./col -->
			
			 <div class="row">
			
            <div class="col-xs-12">
			    <div class="box">
			    

                    <div class="box-header">

					<h1>Most articles</h1>
                       

                    </div><!-- /.box-header -->

                    <div class="box-body">

                        <table id="example1" class="table table-bordered table-striped">

                            <thead>

                            <tr>

                                <th>#</th>

                                <th>Title</th>
                                

                            </tr>

                            </thead>

                            <tbody>

                            <?php $count = 1;?>



                            @foreach($mostarticles as $article)

                                <tr>

                                    <td>{{$count}}</td>

                                    <td>{{$article->title}}</td>
                                    


                                </tr>



                                <?php $count++;?>

                            @endforeach


                            </tbody>

                            <tfoot>

                            <tr>

                                <th>#</th>

                                <th>Title</th>
                                


                            </tr>

                            </tfoot>

                        </table>

                    </div><!-- /.box-body -->


                </div><!-- /.box -->
                </div><!-- /.col -->
                </div><!-- /.row -->
		  
		  
		  
		  
		  
		  

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  @endsection
  
