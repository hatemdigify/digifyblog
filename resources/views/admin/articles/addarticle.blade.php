@extends('admin/layouts/main')

@section('content')


        <!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

			{{\Auth::user()->blogname}}
            <small> Add Article</small>


        </h1>


    </section>


    <!-- Main content -->

    <section class="content">


        <!-- Your Page Content Here -->


        <div class="row">


            <!-- right column -->

            <div class="col-md-12">

                <!-- Horizontal Form -->

                <div class="box box-info">


                    <!-- form start -->


                    @include('errors.list')



                    {!!Form::open(array(
                            'url'=>'admin/articles',
                            'class'=>'form-horizontal',
                            'role'=>'form',
                            'method' => 'POST',
                            'files'=>true
                            ))!!}


                    <div class="row">

                        <div class="col-md-11">

                            <div class="box-body">


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Title</label>


                                    <div class="col-sm-10">

                                        <input type="text" name="title" value="{{ old('title') }}" class="form-control"
                                               placeholder="Title">

                                    </div>

                                </div>


                                <div class="form-group">


                                    <label class="col-sm-2 control-label"> Image</label>

                                    <div class="col-sm-10">

                                        <input type="file" name="image" id="input-id" class="file"
                                               data-preview-file-type="text">

                                    </div>

                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Body</label>

                                    <div class="col-sm-10">
                                        <textarea class="editor22" name="body" rows="20"
                                                  cols="80">{{ old('body') }}</textarea>

                                    </div>

                                </div>


                            </div><!-- /.box-body -->


                        </div><!-- /.col -->

                    </div><!-- /.row -->


                    <div class="row">

                        <div class="col-md-12 ">

                            <div class="box-footer">


                                <button type="submit" class="btn btn-info pull-right">Save</button>

                            </div><!-- /.box-footer -->

                        </div>

                    </div>


                    {!!Form::close()!!}


                </div><!-- /.box-info -->


            </div><!--/.col-md-12(right) -->

        </div>   <!-- /.row -->


    </section><!-- /.content -->

</div><!-- /.content-wrapper -->


@endsection