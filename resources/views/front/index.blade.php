@extends('front/layouts/main')

@section('content')

    <div class="center">

        @foreach($articles as $article)
            <h2><a href="{{url(Request::segment(1).'/article/'.$article->id)}}">{{$article->title}}</a></h2>

            <?php
            $text = App\Digify::limittext($article->body);
            echo $text;
            ?>

            <p class="date">Posted by {{$article->user->name}} <img src="{{asset('assets/front/images/more.gif')}}"
                                                                    alt=""/> <a href="#">Read
                    more</a> <img src="{{asset('assets/front/images/comment.gif')}}" alt=""/> <a href="#">Comments
                    (15)</a> <img src="{{asset('assets/front/images/timeicon.gif')}}" alt=""/> 17.01.</p>
            <br/>
        @endforeach


    </div>



@endsection	
   