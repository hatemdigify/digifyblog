<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Digify Blogs</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <style type="text/css" media="all">
        @import "{{asset('assets/front/images/style.css')}}";
    </style>
</head>
<body>
<div class="content">
    <div id="header">
        <div class="title">
            <h1>Digify Blogs</h1>

            <h3>awesome blog world</h3>
        </div>
    </div>

    <div id="main">


        <div class="center">
            <h1>Digify Blogs Latest blog</h1>
            @foreach($blogs as $blog)
                <h2><a href="{{url('/'.$blog->blogname)}}">{{$blog->blogname}}</a></h2>


            @endforeach


        </div>
        <div class="leftmenu">
            <div class="nav">
                <ul>
                    <li><h4>Our latest blog</h4></li>
                    @foreach($blogs as $blog)

                        <li><a href="{{url('/'.$blog->blogname)}}">{{$blog->blogname}}</a></li>
                    @endforeach

                </ul>
            </div>
        </div>

    </div>
    <div id="prefooter">

    </div>

    <div id="footer">
        <div class="padding"> Copyright &copy; 2006 Your Site Name | Design: <a
                    href="http://www.free-css-templates.com">David Herreman </a> | <a href="#">Contact</a> | <a
                    href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> and <a
                    href="http://validator.w3.org/check?uri=referer">XHTML</a> | <a href="http://www.solucija.com">Solucija.com</a>
            | <a href="#">Login</a></div>
    </div>
</div>
</body>
</html>
     

   