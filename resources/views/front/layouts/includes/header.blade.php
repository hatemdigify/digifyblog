<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>{{$user->blogname}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <style type="text/css" media="all">
        @import "{{asset('assets/front/images/style.css')}}";
    </style>
</head>
<body>
<div class="content">
    <div id="header">
        <div class="title">
            <h1>{{$user->blogname}}</h1>

            <h3>Digify Blogs</h3>
        </div>
    </div>
	
	<div id="main">
	