 @inject('digify','App\Digify')
	
 <div class="leftmenu">
            <div class="nav">
                <ul>
				@foreach($digify->tenarticles(Request::segment(1)) as $ten)
				
				<li><a href="{{url(Request::segment(1).'/article/'.$ten->id)}}">{{$ten->title}}</a></li>
				@endforeach
                    
                </ul>
            </div>
        </div>
    </div>

 <div id="prefooter">
       
    </div>
    <div id="footer">
        <div class="padding"> Copyright &copy; 2006 Your Site Name | Design: <a
                    href="http://www.free-css-templates.com">David Herreman </a> | <a href="#">Contact</a> | <a
                    href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> and <a
                    href="http://validator.w3.org/check?uri=referer">XHTML</a> | <a href="http://www.solucija.com">Solucija.com</a>
            | <a href="#">Login</a></div>
    </div>
</div>
</body>
</html>
