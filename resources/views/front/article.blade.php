@extends('front/layouts/main')

@section('content')

    <div class="center">


        <h2><a href="#">{{$article->title}}</a></h2>


        {!!$article->body!!}
        <p class="date">Posted by {{$article->user->name}} <img src="{{asset('assets/front/images/more.gif')}}" alt=""/>
            <a href="#">Read
                more</a> <img src="{{asset('assets/front/images/comment.gif')}}" alt=""/> <a href="#">Comments
                (15)</a> <img src="{{asset('assets/front/images/timeicon.gif')}}" alt=""/> 17.01.</p>
        <br/>


    </div>



@endsection	
   