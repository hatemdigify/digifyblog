
		<!-- validation -->
   
		@if($errors->any())
		<ul>
		@foreach ($errors->all() as $error)
			<div class="container-fluid">
			<div class="alert alert-dismissable alert-danger  ">
				<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
				<h4>{{$error}}</h4>					
				</div>
			</div>					
			 @endforeach		
		</ul>
		
		@endif
		
		
		<!-- save -->	
		
		@if(!empty(Session::get('save')))
		<div class="container-fluid">
		<div class="alert alert-dismissable alert-success">
		<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
		<h4>{!!Session::get('save') !!}</h4>
			
		</div>
		</div>
		@endif
		
				
		<!-- error -->
		@if(!empty(Session::get('error')))
		<div class="container-fluid">
		<div class="alert alert-dismissable alert-danger">
		<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
			<h4>{!!Session::get('error') !!}</h4>
			
		</div>
		</div>
		@endif
			   