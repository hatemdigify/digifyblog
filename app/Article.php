<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $fillable = [
        'title', 'body', 'image',
    ];

    public function user()
    {
        Return $this->belongsTo('App\User');
    }

}
