<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests;
use App\User;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($blogname)
    {
        //
        $user = User::where('blogname', $blogname)->first();

        $articles = Article::where('user_id', $user->id)->get();

        return view('front.index', compact('articles', 'user'));

    }


    public function article($blogname, $id)
    {
        //

        $user = User::where('blogname', $blogname)->first();
        $article = Article::find($id);
        $article->increment('view');
        return view('front.article', compact('article', 'user'));
    }

}
