<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;

class FrontController extends Controller
{
    //
    public function index()
    {
        $blogs = User::orderBy('id', 'desc')->take(10)->get();
        return view('front.home', compact('blogs'));
    }
}
