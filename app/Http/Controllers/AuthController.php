<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignupRequest;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //

    public function getSignup()
    {
        return View('admin.layouts.signup');
    }

    public function postSignup(SignupRequest $request)
    {
        $user = new User($request->except('_token', 'password'));
        $user->password = Hash::make($request->password);
        if ($user->save()) {
            \Auth::login($user);
            return \Redirect::to('/auth/login');
        } else {
            return \Redirect::back()->withErrors($user->errors());
        }

    }

    /**
     * show login form
     */
    public function getLogin()
    {
        if (Auth::check()) {
            return \Redirect::to('/admin/home');
        } else {
            return view('admin.layouts.login');
        }

    }

    public function postLogin(LoginRequest $request)
    {
        if (\Auth::attempt(['email' => $request->email, 'password' => $request->password]))
            return \Redirect::to('/admin/home');
        else
            return redirect()->back()->with('message', 'Invalid username or password');
    }

    public function getLogout()
    {
        Auth::logout();
        return \Redirect::to('auth/login');

    }
}
