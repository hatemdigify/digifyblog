<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests;
use App\Http\Requests\CreateArticleRequest;
use App\Http\Requests\EditArticleRequest;
use Auth;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $articles = Article::where('user_id', Auth::user()->id)->get();
        return View('admin.articles.articles', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View('admin.articles.addarticle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateArticleRequest $request)
    {
        //
        $article = new Article($request->except('_token', 'image'));
        $article->user_id = Auth::user()->id;

        //upload image
        if ($request->hasFile('image')) {

            $path = public_path();

            $destinationPath = $path . '/uploads/articles'; // upload path

            $image = $request->file('image');

            $extension = $image->getClientOriginalExtension(); // getting image extension

            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image

            $image->move($destinationPath, $name); // uploading file to given path

            $article->image = $name;
        }
        if ($article->save()) {
            \Session::flash('save', 'Article saved successfully');
            return \Redirect::to('/admin/articles');
        } else {
            return \Redirect::back()->withErrors($article->errors());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $articleEdit = Article::find($id);
        return View('admin.articles.editarticle', compact('articleEdit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditArticleRequest $request, $id)
    {
        //
        $article = Article::find($id);
        $article->update($request->except('_token', 'image'));
        //upload image
        if ($request->hasFile('image')) {
            $path = public_path();
            $destinationPath = $path . '/uploads/articles/'; // upload path
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $image->move($destinationPath, $name); // uploading file to given path
            $deletepath = $destinationPath . $article->image;
            //delete old image
            if (file_exists($deletepath) and $article->image != '') {
                unlink($deletepath);
            }
            $article->image = $name;
        }

        if ($article->save()) {
            \Session::flash('save', 'Article Saved successfully');
            return \Redirect::to('/admin/articles');
        } else {
            return \Redirect::back()->withErrors($article->errors());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $article = Article::find($id);
        $deletepath = public_path() . '/uploads/articles/' . $article->image;
        if (file_exists($deletepath) and $article->image != '') {
            unlink($deletepath);
        }
        if ($article->delete()) {
            \Session::flash('save', 'Article delete successfully');
            return \Redirect::to('/admin/articles');
        } else {
            return \Redirect::back()->withErrors($article->errors());
        }


    }

    public function home()
    {
        $noofarticles = Article::where('user_id', Auth::user()->id)->count();
        $mostarticles = Article::where('user_id', Auth::user()->id)->orderBy('view', 'desc')->get();
        return View('admin.home', compact('noofarticles', 'mostarticles'));
    }
}
