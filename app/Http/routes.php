<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    // return Redirect::to('/admin');
//});
Route::get('/', 'FrontController@index');
Route::controller('filemanager', 'FilemanagerLaravelController');
Route::controller('auth', 'AuthController');
Route::resource('/{blogname}/', 'HomeController');
Route::get('/{blogname}/article/{articleid}', 'HomeController@article');

Route::group(array('middleware' => 'auth', 'prefix' => 'admin'), function () {


    Route::get('/home', 'ArticlesController@home');
    Route::resource('/articles', 'ArticlesController');
});
