<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SignupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return !\Auth::check();
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'name' => 'required|unique:users',
			'blogname' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
            
        ];
    }

    function response(Array $errors)
    {
        return \Redirect::back()->withErrors($errors);
    }

    
}
