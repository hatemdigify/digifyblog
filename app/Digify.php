<?php

namespace App;


class Digify
{
    //
    public function tenarticles($request)
    {
        $user = User::where('blogname', $request)->first();
        $tenarticles = Article::where('user_id', $user->id)->take('10')->get();
        return $tenarticles;
    }

    static function limittext($text)
    {
        $text = strip_tags($text);
        $text = trim($text);
        $text = mb_substr($text, 0, 90);
        $text = $text . '..';
        return $text;

    }


}
